<?php

namespace Drupal\menu_save_add_another\Plugin\Menu\LocalAction;

use Drupal\menu_ui\Plugin\Menu\LocalAction\MenuLinkAdd;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Correct the menu add-link redirect.
 */
class MenuLinkAddFixRedirect extends MenuLinkAdd {

  /**
   * {@inheritdoc}
   */
  public function getOptions(RouteMatchInterface $route_match) {
    $options = parent::getOptions($route_match);
    // Remove redirects -- these are handled by the action buttons.
    $options['query'] = [];
    return $options;
  }

}
