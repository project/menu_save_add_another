This module does three things to help out menu administrators:

- Remove the `?destination` parameter from the "+ Add link" button at the top of the menu edit form.
- Add a redirect to the `Save` button on the menu link edit form to take you back to the menu edit form.
- Add a new "Save and Add Another" button on the menu link edit form that redirects to the menu link add form after saving

When stubbing out a menu, the current redirect flow is frustrating, since the editor is bounced back to `/admin/structure/menu` after each menu link is entered. The modification of that redirect and the addition of a button reduces drastically the number of clicks need to generate a bunch of menu links for a menu.